
import os, sys
import toml

from . import log, types

class DiscivideConfig(dict):
    def __init__(self, configfile):
        conf = toml.load(configfile)
        super(DiscivideConfig, self).__init__(conf)
