
from enum import Enum

from discord.ext import commands
from discord.user import BaseUser as DiscordBaseUser

from . import log

class PersistentDataScope(Enum):
    PER_GUILD = 1
    PER_USER = 2
    PER_CHANNEL = 3
    PER_CATEGORY = 7 # category that channel is in defaults to ???
    GLOBAL = 4
    VOLATILE = 5 # in memory, lost on restart
    TMP = 6 # on disk, stored in /tmp style dir

class ConfigError(Exception):
    def __init__(self, msg=''):
        self.message = msg
        Exception.__init__(self, msg)

    def __repr__(self):
        return "ConfigError: %s" % self.message

    __str__ = __repr__

class BaseUser(DiscordBaseUser):
    def __eq__(self, other):
        return self.id == other.id

def StorableBaseUser_from_User(user):
    """Creates a BaseUser that can be pickled for storage."""
    log.info(user._state)
    n_bu = BaseUser(
        state=None,
        data={
            'username': user.name,
            'id': user.id,
            'discriminator': user.discriminator,
            'avatar': user.avatar,
            'bot': user.bot
        }
    )
    return n_bu
