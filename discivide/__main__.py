#!/usr/bin/env python3

# sys
import argparse
import importlib

# via pip
import discord

# internal
from . import log, client, types, config

from . import core

class Discivide(object):
    config = None
    client = None
    config = None

    def __init__(self, configfile):
        log.info("Discivide initializing...")

        self.config = config.DiscivideConfig(configfile)
        log.info(self.config)

        self.client = client.DiscivideBotClient(
            config=self.config,
            command_prefix=self.config['bot']['prefix']
        )

        self.client.load_extension("discivide.core.status")
        self.client.load_extension("discivide.core.extensioncontrol")

        if 'extensions' in self.config['startup']:
            for ext in self.config['startup']['extensions']:
                try:
                    self.client.load_extension(ext)
                except Exception as e:
                    log.err(e)
                    raise e

    def run(self):
        """Blocking call for the main loop.

        Will probably switch to start/stop later...
        """
        if not self.client:
            log.err("Discord Client not initialized!")
            return

        log.info("Discivide starts.")
        self.client.run(self.config['bot']['token'])

        log.info("Discivide stopping...")

if __name__ == "__main__":

    # argument parsing
    parser = argparse.ArgumentParser(description="Discivide")

    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Enable debug logging to terminal"
    )
    parser.add_argument(
        "-c", "--config",
        type=str,
        help="Config file",
        required=True
    )

    args = parser.parse_args()

    if args.verbose:
        log.setLevel(log.DEBUG)

    instance = Discivide(args.config)

    # config file parsing

    # storage backend initialization

    # load command components

    # start serving bot requests
    instance.run()
