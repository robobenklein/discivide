
import discord
from discord.ext import commands
from discivide import checks

class CommandFailureReporter(commands.Cog):
    """Core Discivide Cog to provide error message feedback to users"""

    def __init__(self, bot):
        self.bot = bot

    async def on_command_error(self, ctx, err):
        if isinstance(err, commands.errors.BadArgument):
            await ctx.send("Bad command arguments: %s" % str(err))
        else:
            log.warn(str(err))

def setup(bot):
    bot.add_cog(CommandFailureReporter(bot))
