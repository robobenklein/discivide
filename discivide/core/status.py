
import discord
from discord.ext import commands

class Status(commands.Cog):
    """Status and informational commands relating to the bot itself."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def about(self, ctx):
        """Who is this bot???"""
        whoami = self.bot.user
        appinfo = await self.bot.application_info()
        fulltext = (
            "Hi! I'm {}\n"
            "I'm a bot owned by {}\n"
            "I am running the Discivide framework by Robobenklein\n"
        ).format(
            whoami.name,
            appinfo.owner.mention
        )
        await ctx.send(fulltext)

    @commands.command()
    async def ping(self, ctx):
        """Shows latency to Discord API"""
        await ctx.send("Pong! Latency is %f seconds!" % self.bot.latency)

def setup(bot):
    bot.add_cog(Status(bot))
