
import discord
from discord.ext import commands
from discivide import checks

class ExtensionControl(commands.Cog):
    """Control extensions: load, unload, reload, disable, enable, etc"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["loadext", "ext-load"])
    @commands.check(checks.check_if_bot_owner)
    async def extload(self, ctx, modulename: str):
        """Load an extension during runtime."""
        async with ctx.typing():
            try:
                self.bot.load_extension(modulename)
                await ctx.message.add_reaction('✅')
            except Exception as e:
                await ctx.message.add_reaction('❌')
                await ctx.send("That didn't work! Sorry!")
                fulltext = (
                    "Here's the output:\n"
                    "```\n"
                    "{}: {}\n"
                    "```\n"
                ).format(
                    type(e),
                    e
                )
                await ctx.send(fulltext)
                raise e

    @commands.command(aliases=["reloadext", "ext-reload"])
    @commands.check(checks.check_if_bot_owner)
    async def extreload(self, ctx, modulename: str):
        """Reload a currently loaded extension."""
        async with ctx.typing():
            try:
                self.bot.reload_extension(modulename)
                await ctx.message.add_reaction('✅')
            except Exception as e:
                await ctx.message.add_reaction('❌')
                await ctx.send("I couldn't reload it!")
                fulltext = (
                    "Here's the output:\n"
                    "```\n"
                    "{}: {}\n"
                    "```\n"
                ).format(
                    type(e),
                    e
                )
                await ctx.send(fulltext)
                raise e

    @commands.command(aliases=["unloadext", "ext-unload"])
    @commands.check(checks.check_if_bot_owner)
    async def extunload(self, ctx, modulename: str):
        """Unload a currently loaded extension."""
        async with ctx.typing():
            try:
                self.bot.unload_extension(modulename)
                await ctx.message.add_reaction('✅')
            except Exception as e:
                await ctx.message.add_reaction('❌')
                raise e

def setup(bot):
    bot.add_cog(ExtensionControl(bot))
