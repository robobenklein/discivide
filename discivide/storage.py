
import shelve
import os, errno
import tempfile
import getpass

import discord

from . import log, types

def _mkdir_p(path):
    if not os.path.isdir(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno == errno.EACCES:
                log.err("Permissions: Cannot create dir %s" % os.path.abspath(path))
            raise e

class DiscivideDataShelfManager(shelve.Shelf):
    _guild_shelves = {}
    _user_shelves = {}
    _channel_shelves = {}
    _global_shelf = None
    _volatile_shelf = None
    _temporary_shelf = None
    root_data_dir = None

    def __init__(self, persist_data_dir):
        persist_data_dir = os.path.abspath(persist_data_dir)
        log.info("Initializing DiscivideDataShelfManager using data in %s..." % persist_data_dir)

        # create dirs:
        try:
            _mkdir_p(os.path.join(persist_data_dir))
        except Exception as e:
            log.err("Error creating dirs for DiscivideDataShelfManager!")
            raise e

        self.root_data_dir = persist_data_dir

        self._global_shelf = shelve.open(os.path.join(persist_data_dir, "global_data"))
        self._volatile_shelf = {}
        self._temporary_shelf = shelve.open(os.path.join(
            tempfile.gettempdir(), "discivide-%s" % getpass.getuser()
        ))

    def _get_guild_shelf(self, guild: discord.Guild) -> shelve.Shelf:
        if guild.id in self._guild_shelves:
            return self._guild_shelves[guild.id]

        guild_dir = os.path.join(
            self.root_data_dir, "guild", str(guild.id)
        )
        _mkdir_p(guild_dir)
        with open(os.path.join(guild_dir, "known_as"), 'w') as f:
            f.write(guild.name)
        n_shelf = shelve.open(os.path.join(
            guild_dir, "guild_data"
        ))
        self._guild_shelves[guild.id] = n_shelf
        return n_shelf

    def _get_channel_shelf(self, channel: discord.TextChannel) -> shelve.Shelf:
        if channel.id in self._channel_shelves:
            return self._channel_shelves[channel.id]

        channel_dir = os.path.join(
            self.root_data_dir, "guild", str(channel.guild.id), "channel", str(channel.id)
        )
        _mkdir_p(channel_dir)
        with open(os.path.join(channel_dir, "known_as"), 'w') as f:
            f.write(channel.name)
        n_shelf = shelve.open(os.path.join(
            channel_dir, "channel_data"
        ))
        self._channel_shelves[channel.id] = n_shelf
        return n_shelf

    def _get_user_shelf(self, user: discord.User) -> shelve.Shelf:
        if user.id in self._user_shelves:
            return self._user_shelves[user.id]

        user_dir = os.path.join(
            self.root_data_dir, "users", str(user.id)
        )
        _mkdir_p(user_dir)
        with open(os.path.join(user_dir, "known_as"), 'w') as f:
            f.write(user.name)
        n_shelf = shelve.open(os.path.join(
            user_dir, "user_data"
        ))
        self._user_shelves[user.id] = n_shelf
        return n_shelf

    def get(self, scope: types.PersistentDataScope, key_object = None):
        # either return the existing open shelf accessor or open it

        if scope is types.PersistentDataScope.GLOBAL:
            return self._global_shelf
        elif scope is types.PersistentDataScope.VOLATILE:
            return self._volatile_shelf
        elif scope is types.PersistentDataScope.TMP:
            return self._temporary_shelf
        elif scope is types.PersistentDataScope.PER_GUILD:
            if key_object is None:
                raise ValueError("Need key to get guild shelf.")
            return self._get_guild_shelf(key_object)
        elif scope is types.PersistentDataScope.PER_CHANNEL:
            if key_object is None:
                raise ValueError("Need key to get channel shelf.")
            return self._get_channel_shelf(key_object)
        elif scope is types.PersistentDataScope.PER_USER:
            if key_object is None:
                raise ValueError("Need key to get user shelf.")
            return self._get_user_shelf(key_object)
        else:
            raise ValueError("Unknown data scope!")
