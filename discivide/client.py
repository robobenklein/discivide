
import importlib

import discord
from discord.ext.commands import bot, errors

from . import log, storage

class DiscivideBotClient(bot.Bot):
    """Everything in a Discord.py Bot plus persistent data storage"""
    persistence_manager = None

    def __init__(self, config, *args, **kwargs):
        self.config = config

        # init storage backend:
        self.persistence_manager = storage.DiscivideDataShelfManager(
            self.config['discivide']['persistence_dir']
        )

        kwargs['activity'] = discord.Activity(
            name=self.config['startup']['activity'],
            type=discord.ActivityType[self.config['startup']['activitytype']]
        )
        super(DiscivideBotClient, self).__init__(*args, **kwargs)

    def load_extension(self, name, *args, **kwargs):
        # HACK
        if name in self._BotBase__extensions:
            raise errors.ExtensionAlreadyLoaded(name)

        try:
            lib = importlib.import_module(name, *args, **kwargs)
        except ImportError as e:
            raise errors.ExtensionNotFound(name, e) from e
        else:
            self._load_from_module_spec(lib, name)

    async def on_ready(self):
        log.info("Bot ready.")
        log.debug("Bot name: %s" % self.user.name)
        log.debug("Bot ID: %s" % self.user.id)

    # async def on_error(self, event, *args, **kwargs):
    #     log.err(str(event))

    async def on_message(self, message):
        log.info(str(message))

        await self.process_commands(message)
