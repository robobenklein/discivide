
import discord
from discord.ext import commands

async def check_if_bot_owner(ctx):
    is_owner = await ctx.bot.is_owner(ctx.message.author)
    return is_owner
