
import discord
from discord.ext import commands
from discivide import checks, log

class DiscordActivity(commands.Cog):
    """I can do all sorts of things! In fact, anything you tell me to!"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["setactivity"])
    @commands.check(checks.check_if_bot_owner)
    async def setstatus(self, ctx, kind: str, *, message):
        newact = discord.Activity(
            name=message,
            type=discord.ActivityType[kind]
        )
        await self.bot.change_presence(
            activity=newact
        )

def setup(bot):
    bot.add_cog(DiscordActivity(bot))
