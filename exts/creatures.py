
import random

import discord
from discord.ext import commands
from discivide import checks, log

from .imgur import ImgurURL

class CreatureImages(commands.Cog):
    """Why would you *not* use these commands?"""

    def __init__(self, bot):
        self.bot = bot
        self.imgur = ImgurURL()

    async def post_random_imgur_pic_from_album(self, ctx, imgurlink):
        async with ctx.typing():
            urls = self.imgur.get_imgur_urls(imgurlink)
            il = random.choice(urls)
            log.info("Sending embed of %s" % il)
            em = discord.Embed()
            em.set_image(url=il)
            await ctx.send(
                embed=em
            )

    @commands.command()
    async def wombat(self, ctx):
        await self.post_random_imgur_pic_from_album(ctx, "https://imgur.com/a/mnhzS")
    @commands.command()
    async def capybara(self, ctx):
        await self.post_random_imgur_pic_from_album(ctx, "https://imgur.com/a/hfL80")
    @commands.command()
    async def quokka(self, ctx):
        await self.post_random_imgur_pic_from_album(ctx, "https://imgur.com/a/WvjQA")
    @commands.command()
    async def otter(self, ctx):
        await self.post_random_imgur_pic_from_album(ctx, "https://imgur.com/a/BL2MW")
    @commands.command()
    async def koala(self, ctx):
        await self.post_random_imgur_pic_from_album(ctx, "https://imgur.com/a/BVZkbuB")


def setup(bot):
    bot.add_cog(CreatureImages(bot))
