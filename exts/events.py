
import json
# from dataclasses import dataclass
import datetime
from dateutil import tz
import asyncio

from timefhuman import timefhuman
from fuzzywuzzy import process as fuzzproc
from fuzzywuzzy import fuzz
import discord
from discord.ext import commands

from discivide import log, types, checks

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/New_York')

# @dataclass
class AnEvent(object):
    name: str
    description: str
    creator: types.BaseUser
    created: datetime.datetime
    time: datetime.datetime
    attendees: list

    def __init__(self, name, creator):
        self.name = name
        self.description = ""
        self.setCreator(creator)
        self.created = datetime.datetime.now()
        self.time = None
        self.attendees = []

    def setCreator(self, author):
        self.creator = types.StorableBaseUser_from_User(author)

    async def check_if_event_creator(self, ctx):
        return self.creator.id == ctx.message.author.id

    def is_attendee(self, user):
        u = types.StorableBaseUser_from_User(user)
        return u in self.attendees

    def add_attendee(self, user):
        self.attendees.append(types.StorableBaseUser_from_User(user))

    def remove_attendee(self, user):
        s_user = types.StorableBaseUser_from_User(user)
        return self.attendees.pop(self.attendees.index(s_user))

    def getShortDiscordEmbed(self):
        n_embed = discord.Embed(
            title=self.name,
            description=self.description
        )
        n_embed.set_author(
            name="%s" % str(self.creator)
        )
        if self.time is not None:
            n_embed.timestamp = self.time
        if len(self.attendees) > 0:
            n_embed.add_field(
                name="Number Attending:",
                value=str(len(self.attendees)),
                inline=True
            )
        return n_embed

    def getFullDiscordEmbed(self):
        n_embed = self.getShortDiscordEmbed()
        if len(self.attendees) > 0:
            n_embed.add_field(
                name="Attendees:",
                value='\n'.join([u.mention for u in self.attendees]),
                inline=False
            )
        return n_embed

class EventStorage(object):
    shelf = None
    shelfkey = "eventdata"
    vdict = None
    def __init__(self, ctx):
        self.shelf = ctx.bot.persistence_manager.get(
            types.PersistentDataScope.PER_GUILD,
            ctx.guild
        )
        if self.shelfkey not in self.shelf:
            log.info("Creating new EventStorage")
            self.shelf[self.shelfkey] = {}
        log.info(str(self.shelf))

    def __str__(self):
        return "<class EventStorage(%s events: %s)>" % (
            str(len(self.shelf[self.shelfkey])),
            str(self.shelf[self.shelfkey])[:1910]
        )

    def __enter__(self):
        # log.debug("ENTER: %s" % self)
        self.vdict = self.shelf[self.shelfkey]
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        # log.debug("EXIT: %s" % self)
        self.shelf[self.shelfkey] = self.vdict
        self.shelf.sync()

    async def add(self, eventname, ctx, desc = ""):
        if eventname in self.vdict:
            await ctx.send("Event already exists!")
            raise ValueError("Event already exists!")

        self.vdict[eventname] = AnEvent(eventname, ctx.message.author)
        self.vdict[eventname].description = desc

    def search_events(self, search: str = None):
        sf = lambda e: search.lower() in e.description.lower() or search.lower() in e.name.lower()
        results = sorted(self.vdict.values(), key=lambda k: k.created)
        if search is None:
            return results
        else:
            return list(filter(sf, results))

    async def listevents(self, ctx, search: str = None):
        eventstoshow = self.search_events(search)
        if len(eventstoshow) > 5:
            await ctx.send("Showing 5 most recent events:")
        elif len(eventstoshow) == 0:
            await ctx.send("No events to show!")
        eventstoshow = eventstoshow[:5]
        for event in eventstoshow:
            n_embed = event.getShortDiscordEmbed()

            await ctx.send(embed=n_embed)

    def getSingleEventBySearch(self, event):
        target_event = None
        if event not in self.vdict:
            results = self.search_events(event)
            if len(results) == 1:
                target_event = results[0]
        else:
            target_event = self.vdict[event]
        return target_event

    def getSingleEventFuzzySearch(self, eventsearch):
        res = fuzzproc.extract(
            eventsearch,
            list(self.vdict.keys()),
            limit=2
        )
        if len(res) == 0:
            raise commands.BadArgument(
                message="Couldn't find any event matching %s" % eventsearch
            )
        elif len(res) > 1:
            if res[0][1] < res[1][1] + 10:
                raise commands.BadArgument(
                    message="Not sure which event: '%s' or '%s'" % (
                        res[0][0],
                        res[1][0]
                    )
                )
            else:
                return res[0][0]
        else:
            return res[0][0]

    async def deleteEventPrompt(self, ctx, eventname):
        target_event = self.getSingleEventBySearch(eventname)
        if target_event is None:
            await ctx.send("Not sure what event you're referring to!")
            return

        if not (target_event.check_if_event_creator(ctx) or checks.check_if_bot_owner(ctx)):
            await ctx.send("Only the event creator can delete the event!")
            return

        msg = await ctx.send(
            "Confirm deleting event '{}' by reacting to this message!"
            .format(target_event.name)
        )
        def check(reaction, user):
            return user == ctx.message.author and reaction.message.id == msg.id

        try:
            reaction, user = await ctx.bot.wait_for('reaction_add', check=check, timeout=60.0)
        except asyncio.TimeoutError:
            await ctx.send("Timeout: not deleting event.")
            return
        else:
            del self.vdict[target_event.name]
            await ctx.send('Event deleted!')

    async def clearAllEvents(self):
        self.vdict = {}

class GoingToEvents(commands.Cog):
    """Organize and keep track of who's going to events!"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["createevent", "makeevent", "addevent", "eventadd"])
    async def newevent(self, ctx, *, eventname: str):
        """Create a new event."""
        with EventStorage(ctx) as es:
            await es.add(eventname, ctx)
            await ctx.send(
                "Event created!",
                embed=es.getSingleEventBySearch(eventname).getShortDiscordEmbed()
            )

    @commands.command(aliases=["showevents", "lsevents", "eventlist", "events"])
    async def listevents(self, ctx, *, search: str = None):
        """Show or search for some events."""
        with EventStorage(ctx) as es:
            await es.listevents(ctx, search)

    @commands.command(aliases=["eventdetails", "aboutevent"])
    async def showevent(self, ctx, *, search: str):
        """Show/search for a specific event."""
        with EventStorage(ctx) as es:
            try:
                target_event = es.getSingleEventFuzzySearch(search)
                await ctx.send(
                    "Here's the details:",
                    embed=target_event.getFullDiscordEmbed()
                )
            except types.DiscivideCommandFailure as e:
                await ctx.send(str(e))

    @commands.group(aliases=["eventedit", "changeevent", "eventupdate"])
    async def editevent(self, ctx):
        """Change details about an event."""
        if ctx.invoked_subcommand is None:
            await ctx.send("Tell me what you want to edit! Run help on this command to see the options.")

    @editevent.command(aliases=["when"])
    async def time(self, ctx, eventname: str, *, when: str):
        """Set when an event is happening."""
        with EventStorage(ctx) as es:
            target_event = es.getSingleEventBySearch(eventname)
            if target_event is None:
                await ctx.send("What event are you talking about?")
                return
            try:
                n_time = timefhuman(when)
                n_time.replace(tzinfo=from_zone)
                target_event.time = n_time.astimezone(to_zone)
            except Exception as e:
                await ctx.send("Couldn't understand when! %s" % e)
                log.error(e)
                raise e
            await ctx.send(
                "Updated event time:",
                embed=target_event.getShortDiscordEmbed()
            )

    @editevent.command(aliases=["description"])
    async def desc(self, ctx, eventname: str, *, description: str):
        """Set the description for an event."""
        with EventStorage(ctx) as es:
            target_event = es.getSingleEventBySearch(eventname)
            if target_event is None:
                await ctx.send("What event are you talking about?")
                return
            target_event.description = description
            await ctx.message.add_reaction('👍')

    @commands.command(aliases=["deleteevent", "rmevent", "eventrm", "eventremove", "eventdelete"])
    async def removeevent(self, ctx, *, eventname: str):
        """Delete an event."""
        with EventStorage(ctx) as es:
            await es.deleteEventPrompt(ctx, eventname)

    @commands.command()
    @commands.check(checks.check_if_bot_owner)
    async def event_destroy_all(self, ctx):
        with EventStorage(ctx) as es:
            await es.clearAllEvents()
            await ctx.message.add_reaction('✅')

    @commands.command(aliases=["attending", "willbethere"])
    async def imgoing(self, ctx, *, eventname: str):
        """Add yourself to the attendees list for an event."""
        with EventStorage(ctx) as es:
            target_event = es.getSingleEventBySearch(eventname)
            if target_event is None:
                await ctx.send("No idea what event you're talking about...")
                return
            if target_event.is_attendee(ctx.message.author):
                await ctx.send("You can't go to the same event twice! (%s)" % target_event.name)
                return
            target_event.add_attendee(ctx.message.author)
            await ctx.message.add_reaction('✅')

    @commands.command(aliases=["cancelattendance", "wontbethere", "notattending"])
    async def notgoing(self, ctx, *, eventname: str):
        """Remove yourself from the attendees list for an event."""
        with EventStorage(ctx) as es:
            target_event = es.getSingleEventBySearch(eventname)
            if target_event is None:
                await ctx.send("What event are referring to? ...")
                return
            if target_event.is_attendee(ctx.message.author):
                target_event.remove_attendee(ctx.message.author)
                await ctx.message.add_reaction('✅')
            else:
                await ctx.message.send("You aren't attending that event! (%s)" % target_event.name)

def setup(bot):
    bot.add_cog(GoingToEvents(bot))
