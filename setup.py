import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="discivide",
    version="0.0.1",
    author="Robobenklein",
    author_email="robobenklein@unhexium.net",
    description="Discord divided pluggable bot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/robobenklein/discivide",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6'
)
